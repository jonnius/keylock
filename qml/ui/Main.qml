import QtQuick 2.4
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.Controls.Suru 2.2
import Ubuntu.Content 1.3
import "../backend" 1.0


UITK.MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'enigma.hummlbach'

    width: units.gu(45)
    height: units.gu(75)

    Suru.theme: Suru.System


    StackView {
        id: pageStack
        anchors {
            fill: parent
            bottomMargin: UbuntuApplication.inputMethod.visible ? UbuntuApplication.inputMethod.keyboardRectangle.height/(units.gridUnit / 8) : 0
            Behavior on bottomMargin {
                NumberAnimation {
                    duration: 175
                    easing.type: Easing.OutQuad
                }
            }
        }

        Component.onCompleted: pageStack.push("pages/KeyList.qml")
    }

    Keylock {
        id: keylock
    }

    Connections {
        target: ContentHub
        onShareRequested: {
            var url = String(transfer.items[0].url);
            var text = "";
            var receipient = "";
            for (var i = 0; i < transfer.items.length; i++) {
                if (transfer.items[i].text) {
                    text += String(transfer.items[i].text);
                }
            }
            if (url == null || url == "") {
                url = "file:///home/phablet/.local/share/enigma.hummlbach/contentHubIncoming.txt";
                writeToFile(url, text);
            }
            else if (transfer.items[0].text != null && transfer.items[0].text != "") {
                receipient = transfer.items[0].text;
            }
            pageStack.push("pages/ImportExportPage.qml", {"sharedUrl": url,
                                                          "receipient": receipient});
        }
    }

    function writeToFile(fileUrl, text) {
        var request = new XMLHttpRequest();
        request.open("PUT", fileUrl, false);
        request.send(text);
        return request.status;
    }
}
