import QtQuick 2.9
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3 as UITK
import Ubuntu.Components.Popups 1.3 as UITK_Popups
import QtQuick.Controls.Suru 2.2
import QtQuick.Controls 2.2



Component {
    id: addKeyDialog

    UITK_Popups.Dialog {
        id: dialogue

        property bool isGenerating: false
        property bool isGenerated: false

        title: !isGenerating && !isGenerated ? i18n.tr("Generate Key") : (isGenerating ? i18n.tr("Generating Key") : i18n.tr("Added Key"))

// Stage 1: entering data
        UITK.TextField {
            id: email
            placeholderText: i18n.tr("Enter Email address")
            focus: true
            visible: !isGenerating && !isGenerated
        }

        UITK.TextField {
            id: passphrase
            placeholderText: i18n.tr("Enter a passphrase")
            echoMode: TextInput.Password
            visible: !isGenerating && !isGenerated
        }

        UITK.TextField {
            id: passphraseRepeat
            placeholderText: i18n.tr("Please repeat the passphrase")
            echoMode: TextInput.Password
            visible: !isGenerating && !isGenerated
        }

        Row {
            width: parent.width
            spacing: units.gu(1)

            UITK.Button {
                width: (parent.width - units.gu(1)) / 2
                text: i18n.tr("Cancel")
                onClicked: UITK_Popups.PopupUtils.close(dialogue)
                visible: !isGenerating && !isGenerated
            }

            UITK.Button {
                width: (parent.width - units.gu(1)) / 2
                text: i18n.tr("Create Key")
                color: Suru.highlightColor
                Suru.highlightType: Suru.PositiveHighlight
                enabled: passphrase.text === passphraseRepeat.text && email.text.trim() != ""
                         && /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email.displayText)

                visible: !isGenerating && !isGenerated

                onClicked: {
                    //isGenerating = Qt.binding(function() { return true })
                    isGenerating = true
                    keylock.generateKey(email.text, passphrase.text)
                }
            }
        }


// Stage 2: generating key
        TextEdit {
            text: i18n.tr("Collecting randomness to generate the key, please contribute some entropy by playing around with the indicators for example...")
            wrapMode: TextEdit.WrapAtWordBoundaryOrAnywhere
            readOnly: true
            visible: isGenerating
        }

        UITK.ActivityIndicator {
            id: activity
            visible: isGenerating
            running: isGenerating
        } 


// Stage 3: generation done
        TextEdit {
            text: i18n.tr("Key for %1 has been successfully generated and got the fingerprint %2").arg(email.text).arg(keylock.currentKey ? keylock.currentKey.fingerprint : "")
            wrapMode: TextEdit.WrapAtWordBoundaryOrAnywhere
            readOnly: true
            visible: isGenerated
        }

        UITK.Button {
            width: (parent.width - units.gu(1)) / 2
            text: i18n.tr("Close")
            onClicked: UITK_Popups.PopupUtils.close(dialogue)
            visible: isGenerated
        }

        Connections {
            target: keylock
            onCurrentKeyChanged: {
                isGenerating = false
                isGenerated = true
            }
        }
    }
}
