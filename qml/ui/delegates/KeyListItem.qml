import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import QtQuick.Controls.Suru 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

UITK.ListItem {
    width: parent.width

    signal removeKey()

    property var key: null
    property bool showRemove: true

    leadingActions: UITK.ListItemActions {
        actions: [
            UITK.Action {
                iconName: "delete"
                text: i18n.tr("Remove Key")
                visible: showRemove
                onTriggered: removeKey()
            }
        ]
    }

    /*trailingActions: UITK.ListItemActions {
        actions: [
            UITK.Action {
                iconName: "user-admin"
                text: i18n.tr("Encrypt")
                //onTriggered: AppActions.chat.deleteChatHistory(chat.id)
            },
            UITK.Action {
                iconName: "edit"
                text: i18n.tr("Edit")
            },
            UITK.Action {
                iconName: "share"
                text: i18n.tr("Share")
            },
            UITK.Action {
                iconName: "transfer-progress-upload"
                text: i18n.tr("Upload Public Key")
            }

        ]
    }*/

    ColumnLayout {
        Layout.alignment: Qt.AlignVCenter
        Label {
            id: email
            elide: Text.ElideRight
            text: key.uids.toString()
            anchors {
                left: parent.left
                top: parent.top
                leftMargin: Suru.units.gu(2)
                topMargin: Suru.units.gu(1)
            }
        }

        Label {
            id: fingerprint
            elide: Text.ElideRight
            text: key.fingerprint
            Layout.alignment: Qt.AlignVCenter
            anchors {
                left: parent.left
                top: email.bottom
                leftMargin: Suru.units.gu(2)
            }
        }
    }
}
